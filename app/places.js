const express = require('express');
const router = express.Router();

const createRouter = db => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM `places`', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.get('/:id', (req, res) => {
        db.query('SELECT * FROM `places` WHERE id=' + req.params.id, function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.post('/', (req, res) => {
        const place = req.body;

        db.query(
            'INSERT INTO `places` (`name_place`, `description_place`) ' +
            'VALUES (?, ?)',
            [place.name_place, place.description_place],
            (error, results) => {
                if (error) throw error;

                console.log(results);
                place.id = results.insertId;
                res.send(place);
            }
        );
    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `places` WHERE id=' + req.params.id, function (error, results) {
            if (error) throw error;

            res.send('Successfully deleted');
        });
    });

    router.put('/:id', (req, res) => {
        const place = req.body;
        let updatedFields = '';

        for(let key in place) {
            updatedFields += `${key}='${place[key]}' `;
        }

        console.log(`UPDATE \`places\` SET ${updatedFields}WHERE id=${req.params.id};`);

        db.query(`UPDATE \`places\` SET ${updatedFields}WHERE id=${req.params.id};`, function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    return router;
};

module.exports = createRouter;