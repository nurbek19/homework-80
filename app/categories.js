const express = require('express');
const router = express.Router();

const createRouter = db => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM `categories`', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.get('/:id', (req, res) => {
        db.query('SELECT * FROM `categories` WHERE id=' + req.params.id, function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.post('/', (req, res) => {
        const category = req.body;

        db.query(
            'INSERT INTO `categories` (`name_category`, `description_category`) ' +
            'VALUES (?, ?)',
            [category.name_category, category.description_category],
            (error, results) => {
                if (error) throw error;

                console.log(results);
                category.id = results.insertId;
                res.send(category);
            }
        );
    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `categories` WHERE id=' + req.params.id, function (error, results) {
            if (error) throw error;

            res.send('Successfully deleted');
        });
    });

    router.put('/:id', (req, res) => {
        const category = req.body;
        let updatedFields = '';

        for(let key in category) {
            updatedFields += `${key}='${category[key]}', `;
        }

        updatedFields.replace(/,\s*$/, "");
        console.log(`UPDATE \`categories\` SET ${updatedFields}WHERE id=${req.params.id};`);

        db.query(`UPDATE \`categories\` SET ${updatedFields}WHERE id=${req.params.id};`, function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    return router;
};

module.exports = createRouter;