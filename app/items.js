const express = require('express');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = db => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM `items`', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.get('/:id', (req, res) => {
       res.send('Single item will be here');
    });

    router.post('/', upload.single('image'), (req, res) => {
        const item = req.body;

        if(req.file) {
            item.image = req.file.filename;
        } else {
            item.image = null;
        }

        db.query(
            'INSERT INTO `items` (`item`, `category_id`, `place_id`, `description`, `date_of_registration`, `image`) ' +
            'VALUES (?, ?, ?, ?, ?, ?)',
            [item.item, item.category_id, item.place_id, item.description, item.date_of_registration, item.image],
            (error, results) => {
                if (error) throw error;

                item.id = results.insertId;
                res.send(item);
            }
        );
    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `items` WHERE id=' + req.params.id, function (error, results) {
            if (error) throw error;

            res.send('Successfully deleted');
        });
    });

    return router;
};

module.exports = createRouter;