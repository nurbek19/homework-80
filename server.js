const express = require('express');
const items = require('./app/items');
const categories = require('./app/categories');
const places = require('./app/places');
const mysql = require('mysql');
const cors = require('cors');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'office'
});




connection.connect((err) => {
    if (err) throw err;

    app.use('/items', items(connection));
    app.use('/categories', categories(connection));
    app.use('/places', places(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});